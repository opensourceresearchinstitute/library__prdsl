
This measurement directory is intended to hold code related to internal measurement of the langlet's overhead.  As of Dec 2013, though, the measurement has gotten out of date with the rest of the code base, so all measurements have been turned off, and the measurement code is inconsistent (IE, doesn't work).

Nonetheless, at some point it will be brought back, and used for debugging and dynamic measurements supplied to the Assigner for load balancing and code choices.