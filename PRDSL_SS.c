/*
 * Copyright 2010  OpenSourceCodeStewardshipFoundation
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#include <PR__include/prqueue.h>
#include <PR__include/prhash.h>

#include <PR__include/PR__SS.h>
#include <PR__include/PR__int.h>
#include <PR__include/PR__PI.h>

#include "PRDSL_int.h"
#include "PRDSL_Request_Handlers.h"

//==========================================================================



//===========================================================================

/*
 */
void
PRDSL__start( SlaveVP *seedSlv )
 { PRDSLLangEnv       *langEnv;
   int32             i;
   PRDSLLangData      *langData;
   PRDSLTaskStub      *VPTaskStub, *parentTaskStub;
   
   langEnv = 
      (PRDSLLangEnv *)PR_SS__create_lang_env( sizeof(PRDSLLangEnv), 
                                         seedSlv, PRDSL_MAGIC_NUMBER);
   
   langEnv->num_cores = PR_SS__give_num_cores();
   
      //seed slave is a thread slave, so make a thread's task stub for it
      // and then make another to stand for the seed's parent task.  Make
      // the parent be already ended, and have one child (the seed).  This
      // will make the dissipate handler do the right thing when the seed
      // is dissipated.
   VPTaskStub = 
    PR_int__create_lang_meta_task_in_slave( sizeof(PRDSLTaskStub), 
          &PRDSL__lang_meta_task_freer, seedSlv, PRDSL_MAGIC_NUMBER );

   VPTaskStub->isEnded = FALSE;
   VPTaskStub->isWaitingForChildTasksToEnd = FALSE;
   
   parentTaskStub =
    PR_int__create_lang_meta_task(sizeof(PRDSLTaskStub), 
                    &PRDSL__lang_meta_task_freer, PRDSL_MAGIC_NUMBER);
   
   parentTaskStub->isEnded = TRUE;
   parentTaskStub->numLiveChildVPs = 1; //so dissipate works for seed
   VPTaskStub->parentTaskStub = parentTaskStub;
      //Note: threadTaskStub and parentTaskStub freed when dissipate seedSlv 
   
      //register the langlet's handlers with PR
   PR_SS__register_assigner(                &PRDSL__assign_work_to_slot, seedSlv, PRDSL_MAGIC_NUMBER );
   PR_SS__register_lang_shutdown_handler(   &PRDSL__handle_shutdown, seedSlv, PRDSL_MAGIC_NUMBER );
   PR_SS__register_lang_data_creator(       &PRDSL__create_lang_data_in_slave, seedSlv, PRDSL_MAGIC_NUMBER );
   PR_SS__register_lang_meta_task_creator(  &PRDSL__create_empty_lang_meta_task_in_slave, seedSlv, PRDSL_MAGIC_NUMBER );
   PR_SS__register_make_slave_ready_fn(     &PRDSL__resume_slave, seedSlv, PRDSL_MAGIC_NUMBER );
   PR_SS__register_make_task_ready_fn(      &PRDSL__make_task_ready, seedSlv, PRDSL_MAGIC_NUMBER );

   #ifdef HOLISTIC__TURN_ON_PERF_COUNTERS
   _PRTopEnv->counterHandler = &PR_MEAS__counter_handler;
   PR_MEAS__init_counter_data_structs_for_lang( seedSlv, PRDSL_MAGIC_NUMBER );
   #endif


      //create the ready queues, hash tables used for matching and so forth
   langEnv->slaveReadyQ         = makePrivQ();
   langEnv->taskReadyQ          = makePrivQ();
      
   langEnv->nextCoreToGetNewSlv = 0;
   

   #ifdef HOLISTIC__TURN_ON_OBSERVE_UCC
   langEnv->unitList = makeListOfArrays(sizeof(Unit),128);
   langEnv->ctlDependenciesList = makeListOfArrays(sizeof(Dependency),128);
   langEnv->commDependenciesList = makeListOfArrays(sizeof(Dependency),128);
   langEnv->dynDependenciesList = makeListOfArrays(sizeof(Dependency),128);
   langEnv->ntonGroupsInfo = makePrivDynArrayOfSize((void***)&(langEnv->ntonGroups),8);
   
   langEnv->hwArcs = makeListOfArrays(sizeof(Dependency),128);
   memset(langEnv->last_in_slot,0,sizeof(NUM_CORES * NUM_ANIM_SLOTS * sizeof(Unit)));
   #endif

   MEAS__Make_Meas_Hists_for_PRDSL(seedSlv, PRDSL_MAGIC_NUMBER);
 }



/*This runs inside the MasterVP.
 *It shuts down the langlet
 * Frees any memory allocated by PRDSL__init() and deletes any slaves or tasks
 * still in ready Qs.
 * 
 *Note: it needs the special lang-shutdown-request-handler prototype
 */
 void
PRDSL__handle_shutdown( void *_langEnv )
 { PRDSLLangEnv *langEnv = (PRDSLLangEnv *)_langEnv;
    
   #ifdef HOLISTIC__TURN_ON_OBSERVE_UCC
   //UCC
   FILE* output;
   int n;
   char filename[255];    
    for(n=0;n<255;n++)
    {
        sprintf(filename, "./counters/UCC.%d",n);
        output = fopen(filename,"r");
        if(output)
        {
            fclose(output);
        }else{
            break;
        }
    }
   if(n<255){
    printf("Saving UCC to File: %s ...\n", filename);
    output = fopen(filename,"w+");
    if(output!=NULL){
        set_dependency_file(output);
        //fprintf(output,"digraph Dependencies {\n");
        //set_dot_file(output);
        //FIXME:  first line still depends on counters being enabled, replace w/ unit struct!
        //forAllInDynArrayDo(_PRTopEnv->counter_history_array_info, &print_dot_node_info );
        forAllInListOfArraysDo(langEnv->unitList, &print_unit_to_file);
        forAllInListOfArraysDo( langEnv->commDependenciesList, &print_comm_dependency_to_file );
        forAllInListOfArraysDo( langEnv->ctlDependenciesList, &print_ctl_dependency_to_file );
        forAllInDynArrayDo(langEnv->ntonGroupsInfo,&print_nton_to_file);
        //fprintf(output,"}\n");
        fflush(output);

    } else
        printf("Opening UCC file failed. Please check that folder \"counters\" exists in run directory and has write permission.\n");
   } else {
       printf("Could not open UCC file, please clean \"counters\" folder. (Must contain less than 255 files.)\n");
   }
   //Loop Graph
   for(n=0;n<255;n++)
    {
        sprintf(filename, "./counters/LoopGraph.%d",n);
        output = fopen(filename,"r");
        if(output)
        {
            fclose(output);
        }else{
            break;
        }
    }
   if(n<255){
    printf("Saving LoopGraph to File: %s ...\n", filename);
    output = fopen(filename,"w+");
    if(output!=NULL){
        set_dependency_file(output);
        //fprintf(output,"digraph Dependencies {\n");
        //set_dot_file(output);
        //FIXME:  first line still depends on counters being enabled, replace w/ unit struct!
        //forAllInDynArrayDo(_PRTopEnv->counter_history_array_info, &print_dot_node_info );
        forAllInListOfArraysDo( langEnv->unitList, &print_unit_to_file );
        forAllInListOfArraysDo( langEnv->commDependenciesList, &print_comm_dependency_to_file );
        forAllInListOfArraysDo( langEnv->ctlDependenciesList, &print_ctl_dependency_to_file );
        forAllInListOfArraysDo( langEnv->dynDependenciesList, &print_dyn_dependency_to_file );
        forAllInListOfArraysDo( langEnv->hwArcs, &print_hw_dependency_to_file );
        //fprintf(output,"}\n");
        fflush(output);

    } else
        printf("Opening LoopGraph file failed. Please check that folder \"counters\" exists in run directory and has write permission.\n");
   } else {
       printf("Could not open LoopGraph file, please clean \"counters\" folder. (Must contain less than 255 files.)\n");
   }
   
   
   freeListOfArrays(langEnv->unitList);
   freeListOfArrays(langEnv->commDependenciesList);
   freeListOfArrays(langEnv->ctlDependenciesList);
   freeListOfArrays(langEnv->dynDependenciesList);
   
   #endif
#ifdef HOLISTIC__TURN_ON_PERF_COUNTERS    
    for(n=0;n<255;n++)
    {
        sprintf(filename, "./counters/Counters.%d.csv",n);
        output = fopen(filename,"r");
        if(output)
        {
            fclose(output);
        }else{
            break;
        }
    }
    if(n<255){
    printf("Saving Counter measurements to File: %s ...\n", filename);
    output = fopen(filename,"w+");
    if(output!=NULL){
        int i;
        set_counter_file(output);
        for( i=0; i<NUM_CORES; i++ )
         {
           forAllInListOfArraysDo( langEnv->counterList[i], &PR_MEAS__print_counter_event_to_file );
           fflush(output);
         }

    } else
        printf("Opening UCC file failed. Please check that folder \"counters\" exists in run directory and has write permission.\n");
   } else {
       printf("Could not open UCC file, please clean \"counters\" folder. (Must contain less than 255 files.)\n");
   }
    
#endif
   
   //Things to free:
   // tasks and slaves in ready Qs
   // ready Qs
   //Tasks created but not yet made active, or active but still have
   // unresolved propendents
   PrivQueueStruc *queue;
   SlaveVP        *slave;
   PRDSLTaskStub  *task;
   
   //Not clear what to do with ready slaves still in readyQ.. 
   //They could have been created by a different language and just
   // happened to do a construct from this one.  So it's reasonable 
   // send them over to PRServ instead of ending them.
   //Decided to send ready slaves over to PRServ
   queue = langEnv->slaveReadyQ;
   slave = readPrivQ( queue );
   while( slave != NULL )
    { PR_PI__resume_slave_in_PRServ( slave ); //recycler is for all of PR
      slave = readPrivQ( queue );
    }
   freePrivQ( queue );
   
   //delete any tasks in the readyQ
   queue = langEnv->taskReadyQ;
   task = readPrivQ( queue );
   while( task != NULL )
    { PRDSL__free_ready_but_waiting_task( task );
      task = readPrivQ( queue );
    }
   freePrivQ( queue );
   //Q: how to find tasks created by still waiting for propendents?
   
   //FIXME
   //BUG    -- can be tasks waiting around that go un-freed

   //Need to free:
   // waiting tasks that still have live children -- can't resume after lang shutdown
   // tasks that ended, but still have live children -- unless can still have the cleanup code available when those children end..
   // tasks that ended and have no live children but may still
   //  be named as propendents
   // tasks that were created but not yet made active
   // tasks that were made active but still have unresolved propendents
   
   //Q: when shutdown a lang, want all tasks and slaves created by it
   //  to be freed?  Definitely want ones that are blocked and can only
   //  be unblocked by that language to be freed.  But ones that are
   //  live and may have been suspended by a different language?  Seems
   //  may want to keep those..?
   //A: perhaps provide separate "clean up" calls -- different
   //   variations -- call that first, then call lang shutdown, and
   //   have lang shutdown not try to look for nor deal with live tasks
   //   and/or VPs it created -- the "clean up" call dealt with those.
   
   //Q: can a task created by this lang wait on liveness of tasks created from within this one, but by calls to other language's create requests?
 }


