/*
 * Copyright 2010  OpenSourceCodeStewardshipFoundation
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#include <PR__include/prqueue.h>
#include <PR__include/prhash.h>

#include <PR__include/PR__WL.h>
#include <PR__include/PR__PI.h>

#include "PRDSL_int.h"
#include "PRDSL_Request_Handlers.h"

//#include "PR_impl/Services_Offered_by_PR/Measurement_and_Stats/MEAS__Counter_Recording.h"
//==========================================================================
void
PRDSL__init_Helper();

SlaveVP *
PRDSL__create_thread_w_ID_and_affinity( BirthFnPtr fnPtr,   void *initData, 
                    int32 *thdID, int32 coreToAssignOnto, SlaveVP *creatingThd );

//==========================================================================



//===========================================================================


/*These are the library functions *called in the application*
 * 
 */



//===========================================================================


//===========================================================================

SlaveVP *
PRDSL__create_thread( BirthFnPtr fnPtr,   void *initData,
                        SlaveVP *creatingThd )
 { 
   return PRDSL__create_thread_w_ID_and_affinity( fnPtr, initData, NO_ID,
                                                        ANY_CORE, creatingThd );
 }

SlaveVP *
PRDSL__create_thread_w_ID( BirthFnPtr fnPtr,   void *initData, int32 *thdID,
                         SlaveVP *creatingThd )
 { 
   return PRDSL__create_thread_w_ID_and_affinity( fnPtr, initData, thdID, 
                                                        ANY_CORE, creatingThd );
 }



SlaveVP *
PRDSL__create_thread_w_ID_and_affinity( BirthFnPtr fnPtr,   void *initData, 
                    int32 *thdID, int32 coreToAssignOnto, SlaveVP *creatingThd )
 { PRDSLLangReq reqData;

      //the lang request data is on the stack and disappears when this
      // call returns -- it's guaranteed to remain in the VP's stack for as
      // long as the VP is suspended.
//   reqData.reqType          = create_slave; //know type because in a PR create req
   reqData.coreToAssignOnto = coreToAssignOnto;
   reqData.birthFn            = fnPtr;
   reqData.initData         = initData;
   
   PR_WL__send_create_slaveVP_req( &reqData, thdID, (CreateHandler)&PRDSL__handleCreateThd,
                                                creatingThd, PRDSL_MAGIC_NUMBER );
   return (SlaveVP *)creatingThd->dataRetFromReq;
 }

/*This is always the last thing done in the code animated by a thread VP.
 * Normally, this would be the last line of the thread's top level function.
 * But, if the thread exits from any point, it has to do so by calling
 * this.
 *
 *It simply sends a dissipate request, which handles all the state cleanup.
 */
void
PRDSL__end_thread( SlaveVP *thdToEnd )
 {    
   //the lang request is null for PRDSL version of end slave 
   PR_WL__send_end_slave_req( NULL, (RequestHandler)&PRDSL__handleDissipateVP, thdToEnd, 
                              PRDSL_MAGIC_NUMBER );
 }



//===========================================================================


//======================= task submit and end ==============================
/*
 */
PRDSLTaskStub *
PRDSL__create_task_ready_to_run( BirthFnPtr birthFn, void *args, SlaveVP *animSlv)
 { PRDSLLangReq  reqData;
   
   reqData.args       = args;
   reqData.callingSlv = animSlv;
   
      //Create task is a special form, so have to pass as parameters, the
      // top-level-fn of task and the data for that fn, plus lang's req,
      // animating slave, and lang's magic number
   PR_WL__send_create_task_req( birthFn, args, &reqData, NO_ID,
                                &PRDSL__handleCreateReadyTask, animSlv, 
                                PRDSL_MAGIC_NUMBER );
   return animSlv->dataRetFromReq;
 }

/*
void
PRDSL__submit_task_with_ID( PRDSLTaskType *taskType, void *args, int32 *taskID, 
                          SlaveVP     *animSlv )
 { PRDSLLangReq  reqData;
 
   reqData.reqType    = create_task;
   
   reqData.taskType   = taskType; //PRDSL info about args, dependencies, etc
   reqData.args       = args;
   reqData.callingSlv = animSlv;
 
   PR_WL__send_create_task_req( taskType->fn, args, &reqData, taskID,
                                &PRDSL__handleSubmitTask, animSlv, PRDSL_MAGIC_NUMBER );
 }
*/

/*This call is the last to happen in every task.  It causes the slave to
 * suspend and get the next task out of the task-queue.  Notice there is no
 * assigner here.. only one slave, no slave ReadyQ, and so on..
 *Can either make the assigner take the next task out of the taskQ, or can
 * leave all as it is, and make task-end take the next task.
 *Note: this fits the case in the new PR for no-context tasks, so will use
 * the built-in taskQ of new PR, and should be local and much faster.
 * 
 *The task-stub is saved in the animSlv, so the request handler will get it
 * from there, along with the task-type which has arg types, and so on..
 * 
 * NOTE: if want, don't need to send the animating SlaveVP around.. 
 * instead, can make a single slave per core, and coreCtrlr looks up the
 * slave from having the core number.
 * 
 *But, to stay compatible with all the other PR languages, leave it in..
 */
void
PRDSL__end_task( SlaveVP *animSlv )
 { PRDSLLangReq  reqData;
   
   //PRDSL has nothing extra to communicate to end task handler, so lang req is NULL
   PR_WL__send_end_task_request( NULL, &PRDSL__handleEndTask, animSlv, PRDSL_MAGIC_NUMBER );
 }


/*Waits for all tasks that are direct children to end, then resumes calling
 * task or thread
 */
void
PRDSL__taskwait(SlaveVP *animSlv)
 {
    PRDSLLangReq  reqData;

//   reqData.reqType      = taskwait;
   reqData.callingSlv   = animSlv;
   
   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRDSL__handleTaskwait, animSlv,
                             PRDSL_MAGIC_NUMBER );
 }

void
PRDSL__wait_for_all_PRDSL_created_work_to_end( SlaveVP *seedSlv )
 {
    PRDSLLangReq  reqData;

//   reqData.reqType      = activity_cease_wait;
   reqData.callingSlv   = seedSlv;
   
   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRDSL__handleWaitForPRDSLWorkToEnd, seedSlv,
                             PRDSL_MAGIC_NUMBER );   
 }



/*This is called by the application -- normally the seed slave.  It causes a
 * request to be sent that frees the PRDSL lang env and all its contents.
 *The seed slave resumes from the built-in PRServ langlet's environment.  It
 * will be PRServ that causes return from this call.
 */
void
PRDSL__shutdown( SlaveVP *seedSlv )
 {
   PR_WL__send_lang_shutdown_request( seedSlv, PRDSL_MAGIC_NUMBER );   
 }

//========================== ============================
//

/*This breaks the isolation of application executable from PR internals
 * because it directly accesses PR data structs inside the application
 * executable..  but okay with that for now, for performance..
 */
inline 
int32 *
PRDSL__give_self_taskID( SlaveVP *animSlv )
 { void *metaTask;
   metaTask = PR_WL__give_lang_meta_task_from_slave( animSlv, PRDSL_MAGIC_NUMBER );
   return PR__give_ID_from_lang_meta_task( metaTask );
 }





