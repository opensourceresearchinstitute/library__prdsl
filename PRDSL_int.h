/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 */

#ifndef _PRDSL_INT_H
#define	_PRDSL_INT_H


//NOTE: this file is copied into each library separately, and
// determines behavior such as debug prints,  so there will be
// several different versions of the compiled library, each with
// a different combination of things turned on versus off..
#include "PR_defs__turn_on_and_off.h"

#include <PR__include/langlets/prdsl_wrapper_library.h>
#include <PR__include/PR__structs__common.h>

#include <PR__include/prqueue.h>
#include <PR__include/prhash.h>

#include <PR__include/Services_offered_by_PR/prdependency.h>

//=====================  Measurement of Lang Overheads  =====================
#include "Measurement/PRDSL_Measurement.h"


//===========================================================================

#define NUM_STRUCS_IN_LANG_ENV 1000


//===========================================================================
/*This header defines everything specific to the PRDSL semantic plug-in
 */
typedef struct _PRDSLLangReq    PRDSLLangReq;
typedef void  (*PRDSLTaskFnPtr )   ( void *, SlaveVP *);

#define NO_ID       NULL
#define ANY_CORE    -1

//===========================================================================

struct _PRDSLLangReq
 { 
   SlaveVP           *callingSlv;
   PRDSLTaskStub     *task;
   PRDSLTaskStub     *propTask;
   PRDSLTaskStub     *depTask;
   void              *args;
   
   PRDSLLangReq      *nextReqInHashEntry;
   
   BirthFnPtr         birthFn;
   void              *initData;
   int32              coreToAssignOnto;
 }
/* PRDSLLangReq */;


typedef struct
 { int32            num_cores;
 
   PrivQueueStruc  *slaveReadyQ; //Shared (slaves not pinned)
   PrivQueueStruc  *taskReadyQ;  //Shared (tasks not pinned)
   
   int32            nextCoreToGetNewSlv;

   
   #ifdef HOLISTIC__TURN_ON_OBSERVE_UCC
   ListOfArrays* unitList;
   ListOfArrays* ctlDependenciesList;
   ListOfArrays* commDependenciesList;
   NtoN** ntonGroups;
   PrivDynArrayInfo* ntonGroupsInfo;
   ListOfArrays* dynDependenciesList;
   Unit last_in_slot[NUM_CORES * NUM_ANIM_SLOTS];
   ListOfArrays* hwArcs;
   #endif

   #ifdef HOLISTIC__TURN_ON_PERF_COUNTERS
   ListOfArrays* counterList[NUM_CORES];
   #endif
 }
PRDSLLangEnv;


typedef struct
 {
 }
PRDSLLangData;
 
//===========================================================================

//=========================  Internal use only  =============================

bool32
PRDSL__assign_work_to_slot( void *_langEnv, AnimSlot *slot );

SlaveVP*
PRDSL__create_slave_helper( BirthFnPtr fnPtr, void *initData,
                          PRDSLLangEnv *langEnv,    int32 coreToAssignOnto );

SlaveVP *
PRDSL__create_slave_with( BirthFnPtr fnPtr, void *initData,
                          SlaveVP *creatingSlv );

SlaveVP *
PRDSL__create_slave_with_affinity( BirthFnPtr fnPtr,    void *initData,
                            SlaveVP *creatingSlv, int32 coreToAssignOnto);

void
PRDSL__cleanup_after_shutdown();

//===========================================================================
#endif	/* _PRDSL_H */

