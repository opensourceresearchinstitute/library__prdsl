/*
 * Copyright 2010  OpenSourceCodeStewardshipFoundation
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <stdlib.h>

#include <PR__include/prqueue.h>
#include "PRDSL_int.h"
#include "PRDSL_Request_Handlers.h"

//=========================== Local Fn Prototypes ===========================

//============================== Assigner ==================================
//
/*The assigner is complicated by having both tasks and explicitly created
 * VPs, and by tasks being able to suspend.
 *It can't use an explicit slave to animate a task because of stack
 * pollution. So, it has to keep the two kinds separate.
 * 
 *Q: one assigner for both tasks and slaves, or separate?
 * 
 *Simplest way for the assigner logic is with a Q for extra empty task
 * slaves, and another Q for slaves of both types that are ready to resume.
 *
 *Keep a current task slave for each anim slot. The request handler manages
 * it by pulling from the extraTaskSlvQ when a task suspends, or else
 * creating a new task slave if taskSlvQ empty. 
 *Assigner only assigns a task to the current task slave for the slot.
 *If no more tasks, then takes a ready to resume slave, if also none of them
 * then dissipates extra task slaves (one per invocation).
 *Shutdown condition is: must have no suspended tasks, and no suspended
 * explicit slaves and no more tasks in taskQ.  Will only have the masters
 * plus a current task slave for each slot.. detects this condition. 
 * 
 *Having the two types of slave is part of having communications directly
 * between tasks, and tasks to explicit slaves, which requires the ability
 * to suspend both kinds, but also to keep explicit slave stacks clean from
 * the junk tasks are allowed to leave behind.
 */
bool32
PRDSL__assign_work_to_slot( void *_langEnv, AnimSlot *slot )
 { SlaveVP     *returnSlv;
   PRDSLLangEnv  *langEnv;
   int32        coreNum, slotNum;
   PRDSLTaskStub *newTaskStub;
  
   coreNum = slot->coreSlotIsOn;
//   slotNum = slot->slotIdx;
   
   langEnv = (PRDSLLangEnv *)_langEnv;
   
      //Check for suspended slaves that are ready to resume
   returnSlv = readPrivQ( langEnv->slaveReadyQ );
   if( returnSlv != NULL )  //Yes, have a slave, so return it.
    { returnSlv->coreAnimatedBy   = coreNum;
      
      PR_PI__put_slave_into_slot( returnSlv, slot );
         //Note: PR uses the return boolean to track how much ready work the
         // lang env has in it..
      goto Success;
    }
   
      //No ready slaves, so check for ready tasks
   newTaskStub = readPrivQ( langEnv->taskReadyQ );
   if( newTaskStub != NULL )
    { 
      PR_int__put_task_into_slot( newTaskStub, slot );   
      goto Success;
    }
   
   //If here, didn't assign any work
 Fail:
   return FALSE; //figure out what to do -- go through holistic code still?

 Success:  //doing gotos to here should help with holistic..

   #ifdef HOLISTIC__TURN_ON_OBSERVE_UCC
   //This no longer works -- should be moved into PR in master
   //This assumes the task has already been assigned to a slave, which happens
   // inside Master..
   if( returnMetaTask == NULL )
    { returnSlv = langEnv->process->idleSlv[coreNum][slotNum]; 
    
         //things that would normally happen in resume(), but these VPs
         // never go there
      returnSlv->numTimesAssignedToASlot++;
      Unit newU;
      newU.vp = returnSlv->slaveNum;
      newU.task = returnSlv->numTimesAssignedToASlot;
      addToListOfArrays(Unit,newU,langEnv->unitList);

      if (returnSlv->numTimesAssignedToASlot > 1)
       { Dependency newD;
         newD.from_vp = returnSlv->slaveNum;
         newD.from_task = returnSlv->numTimesAssignedToASlot - 1;
         newD.to_vp = returnSlv->slaveNum;
         newD.to_task = returnSlv->numTimesAssignedToASlot;
         addToListOfArrays(Dependency, newD, langEnv->ctlDependenciesList);  
       }
      returnMetaTask = returnSlv->metaTasks;
    }
   else //returnSlv != NULL
    { //assignSlv->numTimesAssigned++;
      Unit prev_in_slot = 
         langEnv->last_in_slot[coreNum * NUM_ANIM_SLOTS + slotNum];
      if(prev_in_slot.vp != 0)
       { Dependency newD;
         newD.from_vp = prev_in_slot.vp;
         newD.from_task = prev_in_slot.task;
         newD.to_vp = returnSlv->slaveNum;
         newD.to_task = returnSlv->numTimesAssignedToASlot;
         addToListOfArrays(Dependency,newD,langEnv->hwArcs);   
       }
      prev_in_slot.vp = returnSlv->slaveNum;
      prev_in_slot.task = returnSlv->numTimesAssignedToASlot;
      langEnv->last_in_slot[coreNum * NUM_ANIM_SLOTS + slotNum] =
         prev_in_slot;        
    }
   #endif
/* PR handles work available in lang env and in process..
   if( isEmptyPrivQ(langEnv->slaveReadyQ) &&
       isEmptyPrivQ(langEnv->taskReadyQ) ) 
      PR_int__clear_work_in_lang_env(langEnv);
 */
 
   return TRUE;
 }



//=========================== Helper ==============================
void
PRDSL__resume_slave( SlaveVP *slave, void *_langEnv )
 { PRDSLLangEnv *langEnv = (PRDSLLangEnv *)_langEnv;
 
      //both suspended tasks and suspended explicit slaves resumed with this
   writePrivQ( slave, langEnv->slaveReadyQ );
//PR handles this..   PR_int__set_work_in_lang_env(langEnv);
   
   #ifdef HOLISTIC__TURN_ON_PERF_COUNTERS
/*
   int lastRecordIdx = slave->counter_history_array_info->numInArray -1;
   CounterRecord* lastRecord = slave->counter_history[lastRecordIdx];
   saveLowTimeStampCountInto(lastRecord->unblocked_timestamp);
*/
   #endif
   #ifdef HOLISTIC__TURN_ON_OBSERVE_UCC
   slave->numTimesAssignedToASlot++; //Somewhere here!
   Unit newU;
   newU.vp = slave->slaveNum;
   newU.task = slave->numTimesAssignedToASlot;
   addToListOfArrays(Unit,newU,langEnv->unitList);
   
   if (slave->numTimesAssignedToASlot > 1)
    { Dependency newD;
      newD.from_vp = slave->slaveNum;
      newD.from_task = slave->numTimesAssignedToASlot - 1;
      newD.to_vp = slave->slaveNum;
      newD.to_task = slave->numTimesAssignedToASlot;
      addToListOfArrays(Dependency, newD ,langEnv->ctlDependenciesList);  
    }
   #endif
 }
