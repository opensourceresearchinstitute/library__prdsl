/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 */

#ifndef _PRDSL_MEAS_H
#define	_PRDSL_MEAS_H


#ifdef MEAS__TURN_ON_LANG_MEAS

   //PR defines this as empty, by default, so undefine it here, then define below
   #ifdef MEAS__Make_Meas_Hists_for_PRDSL
   #undef MEAS__Make_Meas_Hists_for_PRDSL
   #endif


//===================  Language-specific Measurement Stuff ===================
//
//
   #define SendFromToHistIdx      1 //note: starts at 1
   #define SendOfTypeHistIdx      2
   #define ReceiveFromToHistIdx   3
   #define ReceiveOfTypeHistIdx   4

   #define MEAS__Make_Meas_Hists_for_PRDSL( slave, magicNum ) \
    do \
    { VCilkLangEnv * \
      protoLangEnv = PR_PI__get_proto_lang_env_from_slave( slave, magicNum ); \
      protoLangEnv->measHistsInfo = \
             makePrivDynArrayOfSize( (void***)&(protoLangEnv->measHists), 200); \
      histInfo = protoLangEnv->measHistsInfo;
      makeAMeasHist( SendFromToHistIdx,   "SendFromTo",    50, 0, 100 ) \
      makeAMeasHist( SendOfTypeHistIdx,   "SendOfType",    50, 0, 100 ) \
      makeAMeasHist( ReceiveFromToHistIdx,"ReceiveFromTo", 50, 0, 100 ) \
      makeAMeasHist( ReceiveOfTypeHistIdx,"ReceiveOfType", 50, 0, 100 ) \
 }while(FALSE); /* macro magic to protect local vars from name collision */

   #define Meas_startSendFromTo \
       int32 startStamp, endStamp; \
       saveLowTimeStampCountInto( startStamp ); 

   #define Meas_endSendFromTo \
       saveLowTimeStampCountInto( endStamp ); \
       addIntervalToHist( startStamp, endStamp, \
                                _PRTopEnv->measHists[ SendFromToHistIdx ] );

   #define Meas_startSendOfType \
       int32 startStamp, endStamp; \
       saveLowTimeStampCountInto( startStamp ); \

   #define Meas_endSendOfType \
       saveLowTimeStampCountInto( endStamp ); \
       addIntervalToHist( startStamp, endStamp, \
                                _PRTopEnv->measHists[ SendOfTypeHistIdx ] );

   #define Meas_startReceiveFromTo \
       int32 startStamp, endStamp; \
       saveLowTimeStampCountInto( startStamp ); \

   #define Meas_endReceiveFromTo \
       saveLowTimeStampCountInto( endStamp ); \
       addIntervalToHist( startStamp, endStamp, \
                                _PRTopEnv->measHists[ ReceiveFromToHistIdx ] );

   #define Meas_startReceiveOfType \
       int32 startStamp, endStamp; \
       saveLowTimeStampCountInto( startStamp ); \

   #define Meas_endReceiveOfType \
       saveLowTimeStampCountInto( endStamp ); \
       addIntervalToHist( startStamp, endStamp, \
                                _PRTopEnv->measHists[ReceiveOfTypeHistIdx ] );

#else //===================== turned off ==========================

   #define MEAS__Make_Meas_Hists_for_PRDSL( slave, magicNum )
   #define Meas_startSendFromTo
   #define Meas_endSendFromTo
   #define Meas_startSendOfType
   #define Meas_endSendOfType
   #define Meas_startReceiveFromTo
   #define Meas_endReceiveFromTo
   #define Meas_startReceiveOfType
   #define Meas_endReceiveOfType

#endif  /* MEAS__TURN_ON_LANG_MEAS */

#endif	/*  */

