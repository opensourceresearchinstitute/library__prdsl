/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 */

#ifndef _PRDSL_REQ_H
#define	_PRDSL_REQ_H

#include "PRDSL_int.h"
#include <PR__include/PR__PI.h>

/*This header defines everything specific to the PRDSL semantic plug-in
 */

SlaveVP *
PRDSL__handleCreateThd( void *_langReq, SlaveVP *requestingSlv, void *_langEnv );
void
PRDSL__handleDissipateVP( void *langReq, SlaveVP *requestingSlv, PRDSLLangEnv *langEnv );

PRDSLTaskStub *
PRDSL__handleCreateReadyTask( PRDSLLangReq *langReq, SlaveVP *slave, PRDSLLangEnv *langEnv );
void
PRDSL__handleEndTask( PRDSLLangReq *langReq, SlaveVP *requestingSlv, PRDSLLangEnv *langEnv );

void
PRDSL__handleTaskwait(PRDSLLangReq *langReq, SlaveVP *requestingSlv, PRDSLLangEnv *langEnv);

//====================================
void
PRDSL__lang_meta_task_freer( void *_langMetaTask );

inline 
void
PRDSL__free_langlet_part_of_lang_meta_task( PRDSLTaskStub *stubToFree );

void
PRDSL__langDataFreer( void *_langData );

PRDSLTaskStub *
PRDSL__create_generic_slave_task_stub_in_slave( SlaveVP *slave );

void
PRDSL__handle_shutdown( void *_langEnv );
void * 
PRDSL__create_lang_data_in_slave( SlaveVP *slave );
void * 
PRDSL__create_empty_lang_meta_task_in_slave( SlaveVP *slave );


void
PRDSL__handleWaitForPRDSLWorkToEnd( PRDSLLangReq *langReq, SlaveVP *requestingSlv, PRDSLLangEnv *langEnv);
void
PRDSL__handleWaitThenShutdown( PRDSLLangReq *langReq, SlaveVP *requestingSlv, PRDSLLangEnv *langEnv);

void
PRDSL__resume_slave( SlaveVP *slave, void *_langEnv );
void
PRDSL__make_task_ready( void *taskStub, void *_langEnv );

#endif	/* _PRDSL_REQ_H */

