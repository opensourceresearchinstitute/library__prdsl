/*
 * Copyright 2010  OpenSourceCodeStewardshipFoundation
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <stdlib.h>


#include <PR__include/prqueue.h>
#include <PR__include/prhash.h>
#include "PRDSL_int.h"
#include "PRDSL_Request_Handlers.h"

#include <PR__include/PR__int.h>
#include <PR__include/PR__PI.h>



//=========================== Local Fn Prototypes ===========================

//==========================================================================
//                           Helpers
//


inline
PRDSLTaskStub *
create_task_stub_simple_copy_args( int32 sizeOfArgs, void **args )
 { void **newArgs;
   PRDSLTaskStub* newStub;
   
      //Create a new task stub, with enough room at end to copy args there
   newStub = (PRDSLTaskStub *)PR_int__create_lang_meta_task( 
    sizeof(PRDSLTaskStub) + sizeOfArgs, &PRDSL__lang_meta_task_freer, PRDSL_MAGIC_NUMBER );
   
   
      //Skip over the task stub, to get ptr to space alloc'd for args copy
   newArgs = (void **)( (uint8 *)newStub + sizeof(PRDSLTaskStub) );
      //Copy the args.. can be more arguments than just the ones 
      // that StarSs uses to control ordering of task execution.
   memcpy( newArgs, args, sizeOfArgs );
   newStub->args                = newArgs;
   newStub->numBlockingProp     = 0;
   newStub->numLiveChildTasks   = 0;
   newStub->numLiveChildVPs     = 0;
   newStub->isEnded             = FALSE;
   newStub->dependentTasksQ     = makePrivQ();
   newStub->isActive            = FALSE;
   newStub->canBeAProp          = TRUE;
   
   return newStub;
 }

inline 
PRDSLTaskStub *
create_task_stub_no_copy_args( void **args )
 { 
   PRDSLTaskStub* newStub;
   
      //Create a new task stub
   newStub = (PRDSLTaskStub *)PR_int__create_lang_meta_task( 
    sizeof(PRDSLTaskStub), &PRDSL__lang_meta_task_freer, PRDSL_MAGIC_NUMBER );
   
   newStub->args                = args;
   newStub->numBlockingProp     = 0;
   newStub->numLiveChildTasks   = 0;
   newStub->numLiveChildVPs     = 0;
   newStub->isEnded             = FALSE;
   newStub->dependentTasksQ     = makePrivQ();
   newStub->isActive            = FALSE;
   newStub->canBeAProp          = TRUE;
   
   return newStub;
 }

void
PRDSL__lang_meta_task_freer( void *_langMetaTask )
 { //no malloc'd structs inside task stub, so nothing to do
 }

void * 
PRDSL__create_empty_lang_meta_task_in_slave( SlaveVP *slave )
 { 
   return (void *)PRDSL__create_generic_slave_task_stub_in_slave( slave );
 }


PRDSLTaskStub *
PRDSL__create_generic_slave_task_stub_in_slave( SlaveVP *slave )
 { PRDSLTaskStub *newStub;
 
   newStub = (PRDSLTaskStub *)PR_int__create_lang_meta_task_in_slave( 
      sizeof(PRDSLTaskStub), &PRDSL__lang_meta_task_freer, slave, 
                                                 PRDSL_MAGIC_NUMBER );
//   newStub->numBlockingProp     = 0; //might want when add dependencies
   newStub->numLiveChildTasks   = 0;
   newStub->numLiveChildVPs     = 0;
   newStub->isEnded             = FALSE;
   //initialize the task dependency stuff, as appropriate for a VP
   newStub->dependentTasksQ     = NULL;
   newStub->isActive            = TRUE;
   newStub->canBeAProp          = FALSE;

   return newStub;
 }

/*Initialize semantic data struct..  this initializer doesn't need any input,
 * but some languages may need something from inside the request that was sent
 * to create a slave..  in that case, just make initializer do the malloc then
 * use the PR_PI__give_lang_data  inside the create handler, and fill in the
 * langData values there.
 */
void * 
PRDSL__create_lang_data_in_slave( SlaveVP *slave )
 { PRDSLLangData *langData;

 
   return PR_PI__create_lang_data_in_slave( sizeof(PRDSLLangData), 
                   &PRDSL__langDataFreer, slave, PRDSL_MAGIC_NUMBER );
 }

void
PRDSL__langDataFreer( void *_langData )
 { //no extra structs have been malloc'd into the lang data, so nothing to do 
 }



//===========================  ==============================

/*Application invokes this via wrapper library, when it explicitly creates a
 * thread with the "PRDSL__create_thread()" command.
 * 
 *Slave creation is a special form, so PR does handling before calling this.
 * It does creation of the new slave, and hands it to this handler.  
 *This handler is registered with PR during PRDSL__start().
 * 
 *So, here, create a task Stub that contains a marker stating this is a thread. 
 * Then, attach the task stub to the slave's meta Task via a PR command.
 * 
 *When slave dissipates, PR will call the registered recycler for the task stub.
 */
//inline 
SlaveVP *
PRDSL__handleCreateThd( void *_langReq, SlaveVP *requestingSlv, void *_langEnv )
 { PRDSLLangReq  *langReq;
   PRDSLTaskStub *taskStub, *parentTaskStub;
   SlaveVP    *newSlv;
   PRDSLLangEnv *langEnv;

   langReq = (PRDSLLangReq *) _langReq;
   langEnv = (PRDSLLangEnv *) _langEnv;
   
      //Note to self: the process and type are set by the Master, which called
      // this handler..  that's why this returns the slave..
   newSlv  = PR_PI__create_slaveVP( langReq->birthFn, langReq->initData );
   
   PRDSL__create_lang_data_in_slave( newSlv );
   
   parentTaskStub = PR_PI__give_lang_meta_task_from_slave( requestingSlv, PRDSL_MAGIC_NUMBER );
   parentTaskStub->numLiveChildVPs += 1;
   
   taskStub = PRDSL__create_generic_slave_task_stub_in_slave(requestingSlv); //used for wait info
   taskStub->parentTaskStub = parentTaskStub;

      //note, semantic data will be initialized by separate, registered 
      // initializer, at the point it is accessed the first time.   

   //================= Assign the new thread to a core ===================
   #ifdef DEBUG__TURN_ON_SEQUENTIAL_MODE
   newSlv->coreAnimatedBy = 0;

   #else
      //Assigning slaves to cores is part of SSR code..
   int32 coreToAssignOnto = langReq->coreToAssignOnto;
   if(coreToAssignOnto < 0 || coreToAssignOnto >= langEnv->num_cores )
    {    //out-of-range, so round-robin assignment
      newSlv->coreAnimatedBy = langEnv->nextCoreToGetNewSlv;

      if( langEnv->nextCoreToGetNewSlv >= langEnv->num_cores - 1 )
         langEnv->nextCoreToGetNewSlv  = 0;
      else
         langEnv->nextCoreToGetNewSlv += 1;
    }
   else //core num in-range, so use it
    { newSlv->coreAnimatedBy = coreToAssignOnto;
    }
   #endif
   //========================================================================
   
   

         DEBUG__printf2(dbgRqstHdlr,"Create from: %d, new VP: %d",
                                    requestingSlv->slaveNum, newSlv->slaveNum)

   #ifdef HOLISTIC__TURN_ON_OBSERVE_UCC
   Dependency newD;
   newD.from_vp = requestingSlv->slaveNum;
   newD.from_task = requestingSlv->numTimesAssignedToASlot;
   newD.to_vp = newSlv->slaveNum;
   newD.to_task = 1;
   addToListOfArrays(Dependency,newD,langEnv->commDependenciesList);   
   #endif

      //For PRDSL, caller needs ptr to created thread returned to it
   requestingSlv->dataRetFromReq = newSlv;
   PR_PI__make_slave_ready( requestingSlv, langEnv );
   PR_PI__make_slave_ready( newSlv,        langEnv );
   
   return newSlv;
 }


/*SlaveVP dissipate -- this is NOT task-end!, only call this to end explicitly
 * created threads
 */
//inline
void
PRDSL__handleDissipateVP( void *langReq, SlaveVP *requestingSlv, PRDSLLangEnv *langEnv )
 { 
   PRDSLTaskStub  *parentTaskStub, *ownTaskStub;
 
         DEBUG__printf1(dbgRqstHdlr,"Dissipate request from processor %d",
                                                     requestingSlv->slaveNum)
             
   ownTaskStub    = PR_PI__give_lang_meta_task_from_slave( requestingSlv, PRDSL_MAGIC_NUMBER );
   parentTaskStub = ownTaskStub->parentTaskStub;
   parentTaskStub->numLiveChildVPs -= 1;  //parent wasn't freed, even if ended
   
      //Thinking add a "don't free this!" flag to meta task and lang data, plus
      // an "is ended" flag..  to their prologs..  PRDSL sets the flag at create
      // clears it in here..  then PR does all the freeing, calling the freer
      // pointed to inside the meta-task/lang-data
      //Abort-end of lang or process can lose meta-tasks and lang-datas have the
      // flag set..  memory leak.. so add linked list of meta-tasks/lang-datas
      // owned by a lang/process..  plus a list of slaves.. free those first, 
      // then free the meta-tasks and lang-datas left over..  maybe leave as
      // a bug for now, fix mem leak later..
   
      //if children still live, keep stub around, and last child will free it (below)
   if( ownTaskStub->numLiveChildTasks   != 0 ||
       ownTaskStub->numLiveChildVPs != 0 )
    {
      PR_PI__set_no_del_flag_in_lang_meta_task( ownTaskStub );
      ownTaskStub->isEnded = TRUE; //for children to see, when they end
    }
   
      //check if this is last child of ended parent (note, not possible to
      // have more than one level of ancestor waiting to be freed)
   if( parentTaskStub->isEnded &&
       parentTaskStub->numLiveChildTasks   == 0 && 
       parentTaskStub->numLiveChildVPs == 0 )
    { PR_PI__free_lang_meta_task( parentTaskStub ); //free whole meta-task
    }
   
      //Now, check on parents waiting on child threads to end
   if( parentTaskStub->isWaitingForChildThreadsToEnd &&
       parentTaskStub->numLiveChildVPs == 0 )
    { parentTaskStub->isWaitingForChildThreadsToEnd = FALSE;
      if( parentTaskStub->isWaitingForChildTasksToEnd )
        return; //still waiting on tasks (should be impossible)
      else //parent free to resume
        PR_PI__make_slave_ready( PR_PI__give_slave_lang_meta_task_is_assigned_to(parentTaskStub), langEnv );
    }
   

 FreeSlaveStateAndReturn:
      //PR frees slave's base state, and also the meta-tasks and lang data,
      // except ones have "don't delete" flag set
   return; 
 }



/*Create a task that will subsequently have dependents and/or propendents
 * attached.
 *Save the task that is returned, and give it to the calls that 
 * establish propendent and dependents, and the call that makes a 
 * task "active", which means it's available to be scheduled, and so
 * execute once the assigned propendents are all satisfied.
 */
void
PRDSL__handleCreateInactiveTask( PRDSLLangReq *langReq, SlaveVP *slave, PRDSLLangEnv *langEnv )
 { 
   void             **args;
   PRDSLTaskStub     *taskStub, *parentTaskStub;
//   PRDSLTaskType     *taskType;   
 
   args     = langReq->args;
//   taskType = langReq->taskType; //this is a PRDSL task type struct
   taskStub = create_task_stub_no_copy_args( args );
         
   parentTaskStub = (PRDSLTaskStub *)PR_PI__give_lang_meta_task_from_slave(langReq->callingSlv, PRDSL_MAGIC_NUMBER);
   taskStub->parentTaskStub = parentTaskStub; 
   parentTaskStub->numLiveChildTasks += 1;
   
         DEBUG__printf(dbgRqstHdlr,
                       "Submit req from slaveNum: %d, from task: %d, for task: %d", 
                       langReq->callingSlv->slaveNum, 
                       ((int32 *)PR__give_ID_from_lang_meta_task( parentTaskStub ))[1],
                       ((int32 *)(PR__give_ID_from_lang_meta_task( taskStub)))[1] );
             
      //resume the parent, creator
   langReq->callingSlv->dataRetFromReq = taskStub;
   PR_PI__make_slave_ready( langReq->callingSlv, langEnv );
 }


/*This creates a task and immediately puts it into the task ready Q.
 * The task is not returned because it can't be used as a dependent
 * nor propendent for other tasks.
 */
PRDSLTaskStub *
PRDSL__handleCreateReadyTask( PRDSLLangReq *langReq, SlaveVP *slave, 
                              PRDSLLangEnv *langEnv )
 { 
   void             **args;
   PRDSLTaskStub     *taskStub, *parentTaskStub;
 
   args     = langReq->args;
   taskStub = create_task_stub_no_copy_args( args );
         
   parentTaskStub = (PRDSLTaskStub *)PR_PI__give_lang_meta_task_from_slave(langReq->callingSlv, PRDSL_MAGIC_NUMBER);
   taskStub->parentTaskStub = parentTaskStub; 
   parentTaskStub->numLiveChildTasks += 1;
   
//         DEBUG__printf(dbgRqstHdlr,"Create from slaveNum: %d, from task: %d, for task: %d", langReq->callingSlv->slaveNum, parentTaskStub->taskID[1], taskStub->taskID[1])
   
   PR_PI__make_task_ready( taskStub, langEnv );
         
      //resume the parent, creator
   langReq->callingSlv->dataRetFromReq = taskStub;
   PR_PI__make_slave_ready( langReq->callingSlv, langEnv );
   
   return taskStub;
 }



/*Use this to make the dependent task be blocked, waiting for the
 *  propendent task to complete.
 *The wrapper library can make either the dependent or the propendent
 * the "viewpoint", but both call this same handler.
 */
PRDSLTaskStub *
PRDSL__handleConnectPropendentToDependent( PRDSLLangReq *langReq,
                                           SlaveVP *slave,
                                           PRDSLLangEnv *langEnv )
 { 
   PRDSLTaskStub     *depTask, *propTask;
   
   propTask = langReq->propTask;
   depTask = langReq->depTask;
   
   //If the dependent task is already active, and also try to add a
   // new propendent, then can't guarantee the timing.. that's why
   // 'active' flag was added.. so check if dep already active
   if( depTask->isActive ) printf("EXCEPTION! %s, %d", __FILE__,__LINE__);//PR__throw_simple_exception( slave );
   if( propTask->isEnded ) //the propendent has already cleared, do nothing
      return;
   
   pushPrivQ( depTask, propTask->dependentTasksQ );
   depTask->numBlockingProp += 1;
   
      //resume the invoking slave/task
   PR_PI__make_slave_ready( langReq->callingSlv, langEnv );
 }


/*
 */
PRDSLTaskStub *
PRDSL__handleMakeTaskActive( PRDSLLangReq *langReq, SlaveVP *slave, PRDSLLangEnv *langEnv )
 { 
   void             **args;
   PRDSLTaskStub     *taskStub;
   
   langReq->task->isActive = TRUE;
   if( langReq->task->numBlockingProp = 0 )
      PR_PI__make_task_ready( taskStub, langEnv );
             
      //resume the invoking slave/task
   PR_PI__make_slave_ready( langReq->callingSlv, langEnv );
 }

/*
 * -] Make task-end set the "ended" flag inside the meta-task just ended.
    Go down the list of dependents of the just-ended
    task, and update the count inside the 
    dependent task.  If this makes the count become zero, then check
    the "active" flag.  If not set, do nothing.  If set, make
    the dependent task ready. (PR internally handles this task end
    causing a waiting parent to become resumed).
    When done traversing list, delete the list (make it a dyn array or
    list-of-arrays -- can't build in 'cause given task can be depend
    of multiple propendents)
- -] task-end can't delete the meta-task when the task ends!  Must keep it
    around, as there may be a pointer to it held in the DSL-generated
    code that will try to add the ended task as a propendent..

 */
void
PRDSL__handleEndTask( PRDSLLangReq *langReq, SlaveVP *requestingSlv, PRDSLLangEnv *langEnv )
 { PRDSLTaskStub   *endingTaskStub, *depTask, *parentStub;
   PrivQueueStruc  *depQ;
 
   endingTaskStub   = 
    (PRDSLTaskStub *)PR_int__give_lang_meta_task_from_slave( requestingSlv, PRDSL_MAGIC_NUMBER );

   depQ = endingTaskStub->dependentTasksQ;
   endingTaskStub->isEnded = TRUE;
   depTask = readPrivQ( depQ );
   while( depTask != NULL )
    {
      depTask->numBlockingProp -= 1;
      if( depTask->numBlockingProp = 0 && depTask->isActive )
       { PR_PI__make_task_ready( depTask, langEnv );         
       }
      depTask = readPrivQ( depQ );
    }
   freePrivQ( endingTaskStub->dependentTasksQ );
      
//         DEBUG__printf(dbgRqstHdlr,"EndTask req from slaveNum: %d, task: %d",langReq->callingSlv->slaveNum, endingTaskStub->taskID[1])
             
   //free internal structs beyond depQ;
             
   //task ended, so no way for it to gain children if it doesn't
   // have them now.  Also, if happens to gain dependents, they won't
   // be queued up, so, don't need any internal queue structs  nor 
   // other internals of the task.  So, in every case, free the internal
   // structs of the task.  Then, if has no children and marked as
   // cannot be added as a propendent, then free the meta task.  Else,
   // keep the bare task struct around.

   //free meta task now, if no children, and cannot be added as a
   // propendent (if no children, can't get and then nothing to free later.)
   if( endingTaskStub->numLiveChildTasks == 0 &&
       endingTaskStub->numLiveChildVPs  == 0 &&
       endingTaskStub->canBeAProp == FALSE )
    { free( endingTaskStub );
    }
              
      //"wait" functionality: Check if parent was waiting on this task
   parentStub = endingTaskStub->parentTaskStub;
   parentStub->numLiveChildTasks -= 1;
   if( parentStub->isWaitingForChildTasksToEnd && 
       parentStub->numLiveChildTasks == 0)
    {
      parentStub->isWaitingForChildTasksToEnd = FALSE;
      PR_PI__make_slave_ready( PR_PI__give_slave_lang_meta_task_is_assigned_to(parentStub), langEnv );
    }
   
      //Check if parent ended, and this was last descendent, then free it
   if( parentStub->isEnded && 
       parentStub->numLiveChildTasks == 0 &&
       parentStub->numLiveChildVPs == 0 &&
       parentStub->canBeAProp == FALSE )
    { PR_PI__free_lang_meta_task( parentStub );
    }
 }

//==========================================================================


/*This is called during shutdown, to delete a task that still has dependencies
 * unfulfilled.
 *PRDSL has a complex meta task, so this freer has to do a fair amount.
 * The arguments were copied over into the meta task, and there are queues of
 * other tasks waiting on completion of this one
 */
//inline 
void
PRDSL__free_ready_but_waiting_task( void *langMetaTask )
 { PRDSLTaskStub   *endingTaskStub, *depTask;
   PrivQueueStruc  *depQ;
         
   
   endingTaskStub   = (PRDSLTaskStub *)langMetaTask;
   
   depQ = endingTaskStub->dependentTasksQ;

/* Not sure what to do with dependent tasks blocked by the one
 *  being freed at shutdown..  can't free 'cause may be multiple frees then
   depTask = readPrivQ( depQ );
   while( depTask != NULL )
    {
      depTask->numBlockingProp -= 1;
      if( depTask->numBlockingProp = 0 && depTask->isActive )
       { PR_PI__make_task_ready( depTask, langEnv );         
       }
      depTask = readPrivQ( depQ );
    }
*/
   freePrivQ( endingTaskStub->dependentTasksQ );   
 }


/*Frees only the langlet's portion of the meta task.
 *This is given to PR when a meta task is created.  It is also used by the
 * end task handler, which may choose to keep the meta task info far past the
 * end of the task's existence..
 */
//inline 
void
PRDSL__free_langlet_part_of_lang_meta_task( PRDSLTaskStub *stubToFree )
 { if(stubToFree->dependentTasksQ != NULL ) //a thread stub has NULL entry
    { freePrivQ( stubToFree->dependentTasksQ );
    }
 }

//========================== Task Comm handlers ===========================




/*Waits for all tasks that are direct children to end, then resumes calling
 * task or thread
 */
//inline 
void
PRDSL__handleTaskwait( PRDSLLangReq *langReq, SlaveVP *requestingSlv, PRDSLLangEnv *langEnv)
 { PRDSLTaskStub* taskStub;
 
            DEBUG__printf1(dbgRqstHdlr,"Taskwait request from processor %d",
                                                      requestingSlv->slaveNum)
    
   taskStub = (PRDSLTaskStub *)PR_PI__give_lang_meta_task_from_slave( requestingSlv, PRDSL_MAGIC_NUMBER);
   
   if( taskStub->numLiveChildTasks == 0 )
    {    //nobody to wait for, resume
      PR_PI__make_slave_ready( requestingSlv, langEnv );
    }
   else  //have to wait, mark waiting
    {        
      taskStub->isWaitingForChildTasksToEnd = TRUE;
    }    
 }

void
PRDSL__handleWaitForPRDSLWorkToEnd( PRDSLLangReq *langReq, SlaveVP *requestingSlv, PRDSLLangEnv *langEnv)
 {
   PR_PI__handle_wait_for_langlets_work_to_end( requestingSlv, langEnv );
 }

void
PRDSL__handleWaitThenShutdown( PRDSLLangReq *langReq, SlaveVP *requestingSlv, PRDSLLangEnv *langEnv)
 {
   DEBUG__printf(TRUE, "implement me");
 }

void
PRDSL__make_task_ready( void *taskStub, void *_langEnv )
 {
   writePrivQ( taskStub, ((PRDSLLangEnv*)_langEnv)->taskReadyQ );
 }
